package alice.ep.conf;

import io.dropwizard.Configuration;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class RouteConfiguration extends Configuration {

    private String host;
    private int port;
    private String contextPath;

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    @Override
    public String toString() {
        return "RouteConfiguration{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", contextPath='" + contextPath + '\'' +
                '}';
    }
}
