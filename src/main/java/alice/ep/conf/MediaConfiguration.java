package alice.ep.conf;

import io.dropwizard.Configuration;

/**
 * Created by xuhaixiang on 7/27/14.
 */
public class MediaConfiguration extends Configuration {

    private String host;

    private int port;

    private String mp4app;

    private String dataPath;

    private String repositoryPath;

    private String liveapp;

    public String getLiveapp() {
        return liveapp;
    }

    public void setLiveapp(String liveapp) {
        this.liveapp = liveapp;
    }

    public String getMp4app() {
        return mp4app;
    }

    public void setMp4app(String mp4app) {
        this.mp4app = mp4app;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getDataPath() {
        return dataPath;
    }

    public void setDataPath(String dataPath) {
        this.dataPath = dataPath;
    }

    public String getRepositoryPath() {
        return repositoryPath;
    }

    public void setRepositoryPath(String repositoryPath) {
        this.repositoryPath = repositoryPath;
    }

    @Override
    public String toString() {
        return "MediaConfiguration{" +
                "host='" + host + '\'' +
                ", port=" + port +
                ", mp4app='" + mp4app + '\'' +
                ", dataPath='" + dataPath + '\'' +
                ", repositoryPath='" + repositoryPath + '\'' +
                ", liveapp='" + liveapp + '\'' +
                "} " + super.toString();
    }
}
