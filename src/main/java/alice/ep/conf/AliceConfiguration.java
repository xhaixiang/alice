package alice.ep.conf;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class AliceConfiguration extends Configuration {
    @NotEmpty
    private String name = "nils";

    private String host="127.0.0.1";

    private int port = 80;

    private int size = 100;

    private int threshold = 80;

    private String broadcast = "LIVE";

    private String protocol  = "RTMP";

    private String scriptRoot = "/root/scripts";

    public String getScriptRoot() {
        return scriptRoot;
    }

    public void setScriptRoot(String scriptRoot) {
        this.scriptRoot = scriptRoot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(String broadcast) {
        this.broadcast = broadcast;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    @Override
    public String toString() {
        return "AliceConfiguration{" +
                "name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", size=" + size +
                ", threshold=" + threshold +
                ", broadcast='" + broadcast + '\'' +
                ", protocol='" + protocol + '\'' +
                ", scriptRoot='" + scriptRoot + '\'' +
                "} " + super.toString();
    }
}
