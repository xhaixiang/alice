package alice.ep.conf;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class ServerConfiguration extends Configuration {

    @JsonProperty("alice")
    AliceConfiguration alice;

    @JsonProperty("route")
    RouteConfiguration route;

    @JsonProperty("media")
    MediaConfiguration mediaServer;

    public MediaConfiguration getMediaServer() {
        return mediaServer;
    }

    public void setMediaServer(MediaConfiguration mediaServer) {
        this.mediaServer = mediaServer;
    }

    public AliceConfiguration getAlice() {
        return alice;
    }

    public void setAlice(AliceConfiguration alice) {
        this.alice = alice;
    }

    public RouteConfiguration getRoute() {
        return route;
    }

    public void setRoute(RouteConfiguration route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return "ServerConfiguration{" +
                "alice=" + alice +
                ", route=" + route +
                ", mediaServer=" + mediaServer +
                '}';
    }
}
