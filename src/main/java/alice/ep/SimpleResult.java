package alice.ep;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class SimpleResult {

    private StartStatus status ;

    @JsonProperty
    private String name;

    @JsonProperty
    private String streamUrl;

    public SimpleResult() {
        this("", StartStatus.SUCCESS);
    }

    public SimpleResult(String name) {
        this(name, StartStatus.SUCCESS);
    }

    public SimpleResult(String name, StartStatus s){
        this.name = name;
        this.status = s;
    }

    public SimpleResult(String name, String streamUrl) {
        this.name = name;
        this.streamUrl = streamUrl;
        this.status = StartStatus.SUCCESS;
    }

    public StartStatus getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    @Override
    public String toString() {
        return "SimpleResult{" +
                "status=" + status +
                ", name='" + name + '\'' +
                ", streamUrl='" + streamUrl + '\'' +
                '}';
    }
}
