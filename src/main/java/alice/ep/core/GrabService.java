package alice.ep.core;

import alice.ep.Recording;
import alice.ep.conf.ServerConfiguration;
import alice.ep.startup.RouteService;
import org.apache.commons.io.FileUtils;
import retrofit.RestAdapter;
import tom.script.util.HDFSCmd;
import tom.script.util.RecordCmd;

import java.io.File;

/**
 * Created by xuhaixiang on 9/14/14.
 */
public class GrabService {

    public void recording(Recording recording, ServerConfiguration conf) {

        File cacheFile = saveToCacheStore(recording, conf);

        uploadToHdfs(recording, conf, cacheFile);

        finishRecording(recording, conf);

    }

    private void finishRecording(Recording recording, ServerConfiguration conf) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://" + conf.getRoute().getHost() + ":" + conf.getRoute().getPort() + "/" + conf.getRoute().getContextPath())
                .build();
        RouteService routeService = restAdapter.create(RouteService.class);
        routeService.finishRecording(recording.getName());
    }

    private void uploadToHdfs(Recording recording, ServerConfiguration conf, File cacheFile) {
        HDFSCmd hdfsCmd = new HDFSCmd(conf.getAlice().getScriptRoot());
        hdfsCmd.upload(cacheFile, recording.getPath());
    }

    private File saveToCacheStore(Recording recording, ServerConfiguration conf) {
        RecordCmd cmd = new RecordCmd(conf.getAlice().getScriptRoot());
        String repoPath = conf.getMediaServer().getRepositoryPath();
        File cacheFile = FileUtils.getFile(repoPath, recording.getName());
        cmd.record(recording.getStreamUrl(), cacheFile);
        return cacheFile;
    }
}
