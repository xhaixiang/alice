package alice.ep.core;

import alice.ep.LiveMedia;
import alice.ep.Media;
import alice.ep.conf.ServerConfiguration;
import tom.script.util.PlayCmd;

import java.io.IOException;

public class PlayService {

    public String run(Media media, ServerConfiguration configuration) throws IOException {
        CacheStore cs = new CacheStore();
        cs.play(media, configuration);
        StringBuilder s = new StringBuilder();
        s.append(configuration.getAlice().getProtocol().toLowerCase()).append("://")
                .append(configuration.getMediaServer().getHost()).append(":")
                .append(configuration.getMediaServer().getPort()).append("/")
                .append(configuration.getMediaServer().getMp4app()).append("/")
                .append(media.getName());
        return s.toString();

    }

    public String live(final String duration, final LiveMedia media, final ServerConfiguration configuration) {
        StringBuilder s = new StringBuilder();
        s.append(configuration.getAlice().getProtocol().toLowerCase()).append("://")
                .append(configuration.getMediaServer().getHost()).append(":")
                .append(configuration.getMediaServer().getPort()).append("/")
                .append(configuration.getMediaServer().getLiveapp()).append("/")
                .append(media.getName());
        final String streamUrl = s.toString();
        PlayCmd cmd = new PlayCmd(configuration.getAlice().getScriptRoot());
        cmd.live(duration, media.getStreamUrl(), streamUrl);
        return streamUrl;
    }
}
