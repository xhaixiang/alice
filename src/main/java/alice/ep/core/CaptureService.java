package alice.ep.core;

import alice.ep.LiveMedia;
import alice.ep.Media;
import alice.ep.conf.ServerConfiguration;
import org.apache.commons.lang3.StringUtils;
import tom.script.util.CaptureCmd;

import java.io.File;

public class CaptureService {
    public String capture(Media media, Long timeSlice, ServerConfiguration configuration) {
        File cache = new CacheStore().cache(media, configuration);
        String imagePath = media.getPath() + "/"+ media.getName() + ".png";
        CaptureCmd cmd = new CaptureCmd(configuration.getAlice().getScriptRoot());
        cmd.capture(cache, convert(timeSlice), imagePath);
        return imagePath;
    }


    public String convert(long l) {
        long hour = l / 3600L;
        long hourMod = l % 3600l;
        long minute = hourMod / 60l;
        long second = hourMod % 60l;
        return w(hour) + ":" + w(minute) + ":" + w(second);
    }

    private String w(long second) {
        return StringUtils.leftPad("" + second, 2, "0");
    }

    public String captureLive(LiveMedia media, ServerConfiguration configuration) {
        File cache = new CacheStore().cacheLiveImage(media, configuration);
        CaptureCmd cmd = new CaptureCmd(configuration.getAlice().getScriptRoot());
        cmd.captureLive(cache, media.getStreamUrl());
        return media.getName() + ".png";
    }
}
