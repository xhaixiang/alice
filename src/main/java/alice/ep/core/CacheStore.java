package alice.ep.core;

import alice.ep.LiveMedia;
import alice.ep.Media;
import alice.ep.conf.ServerConfiguration;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tom.script.util.HDFSCmd;
import tom.script.util.PlayCmd;

import java.io.File;
import java.io.IOException;

public class CacheStore {
    private Logger logger = LoggerFactory.getLogger(CacheStore.class);

    public File cache(Media media, ServerConfiguration configuration){
        String repoPath = configuration.getMediaServer().getRepositoryPath();
        File cacheFile = FileUtils.getFile(repoPath, media.getName());
        if (!cacheFile.exists()) {
            logger.info("media not exist in cache, copy from remote {}", media);
            HDFSCmd cmd = new HDFSCmd(configuration.getAlice().getScriptRoot());
            try {
                cmd.downaload(media.getPath(), media.getName(), cacheFile);
            } catch (Exception e) {
                logger.error("play error", e);
            }
        }
        return cacheFile;
    }

    public void play(Media media, ServerConfiguration configuration) throws IOException {
        String rtmpPath = configuration.getMediaServer().getDataPath();
        File mediaFile = FileUtils.getFile(rtmpPath, media.getName());
        if (!mediaFile.exists()) {
            String cachePath = configuration.getMediaServer().getRepositoryPath();
            File cacheFile = FileUtils.getFile(cachePath, media.getName());
            if (!cacheFile.exists()) {
                logger.info("media not exist in cache, copy from remote {}", media);
                HDFSCmd cmd = new HDFSCmd(configuration.getAlice().getScriptRoot());
                try {
                    cmd.downaload(media.getPath(), media.getName(), cacheFile);
                } catch (Exception e) {
                    logger.error("play error", e);
                }
            }
            logger.info("media exist in cache {}, copy to rtmp path", media);
            FileUtils.copyFile(cacheFile, mediaFile);
        } else {
            logger.info("media exist in rtmp path:{} with configure {}", media, configuration);
        }

    }

    public File cacheLiveImage(LiveMedia media, ServerConfiguration configuration) {
        String repoPath = configuration.getMediaServer().getRepositoryPath();
        File cacheFile = FileUtils.getFile(repoPath, media.getName() + ".png");
        return cacheFile;
    }
}
