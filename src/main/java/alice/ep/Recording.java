package alice.ep;

public class Recording {
    private String streamUrl;
    private Long timeDuration;
    private String name;
    private String id;
    private String path;

    public Recording() {
    }

    public Recording(Media media, String streamUrl, Long duration) {
        this.streamUrl = streamUrl;
        this.timeDuration = duration;
        this.name = media.getName();
        this.id = media.getKey();
        this.path = media.getPath();
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getStreamUrl() {
        return streamUrl;
    }

    public void setStreamUrl(String streamUrl) {
        this.streamUrl = streamUrl;
    }

    public Long getTimeDuration() {
        return timeDuration;
    }

    public void setTimeDuration(Long timeDuration) {
        this.timeDuration = timeDuration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Recording{" +
                "streamUrl='" + streamUrl + '\'' +
                ", timeDuration=" + timeDuration +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
