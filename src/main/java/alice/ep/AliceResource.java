package alice.ep;

import alice.ep.conf.ServerConfiguration;
import alice.ep.core.CaptureService;
import alice.ep.core.GrabService;
import alice.ep.core.PlayService;
import com.codahale.metrics.annotation.Timed;
import io.dropwizard.jersey.params.LongParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;

@Path("/alice")
@Produces(MediaType.APPLICATION_JSON)
public class AliceResource {
    private Logger logger = LoggerFactory.getLogger(AliceResource.class);
    private ServerConfiguration configuration;
    CaptureService captureService = new CaptureService();
    PlayService playService = new PlayService();
    GrabService grabService = new GrabService();

    public AliceResource(ServerConfiguration configuration) {
        this.configuration = configuration;
    }

    @POST
    @Path("start")
    @Timed
    public SimpleResult start(Media media, @QueryParam("f") @DefaultValue("origin") String frame) throws IOException {
        logger.info("start media:{}, {}", media, frame);
        SimpleResult result = new SimpleResult(media.getName(), playService.run(media, configuration));
        return result;
    }

    @POST
    @Path("live")
    @Timed
    public SimpleResult live(LiveMedia media, @QueryParam("t") @DefaultValue("3600") LongParam duration, @QueryParam("f") @DefaultValue("origin") String frame) {
        logger.info("live media:{}, {}", media, frame);
        String streamProxyUrl = playService.live(""+duration, media, configuration);
        SimpleResult result = new SimpleResult(media.getName(), streamProxyUrl);
        return result;
    }


    @POST
    @Path("capture/media")
    @Timed
    public SimpleResult capture(Media media, @QueryParam("t") @DefaultValue("300") LongParam timeSlice) {
        logger.info("capture media:{}, {}", media, timeSlice);
        String hdfsPath = captureService.capture(media, timeSlice.get(), configuration);
        SimpleResult result = new SimpleResult(media.getName(), hdfsPath);
        return result;
    }

    @POST
    @Path("capture/live")
    @Timed
    public SimpleResult captureLive(LiveMedia media, @QueryParam("t") @DefaultValue("3600") LongParam timeSlice) {
        logger.info("capture live stream:{}", media);
        String hdfsPath = captureService.captureLive(media, configuration);
        SimpleResult result = new SimpleResult(media.getName(), hdfsPath);
        return result;
    }

    @POST
    @Path("recording")
    @Timed
    public SimpleResult recording(Media media, @QueryParam("s") String streamUrl, @QueryParam("t") @DefaultValue("100") Long duration) {
        logger.info("recording media:{}, {}, {}", media, streamUrl, duration);
        Recording recording = new Recording(media, streamUrl, duration);
        grabService.recording(recording, configuration);
        SimpleResult result = new SimpleResult(recording.getName(), "");
        return result;
    }

}
