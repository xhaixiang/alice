package alice.ep.monitor;

import com.codahale.metrics.health.HealthCheck;

public class DemoHealthCheck extends HealthCheck {

    @Override
    protected HealthCheck.Result check() throws Exception {
        return Result.healthy();
    }
}