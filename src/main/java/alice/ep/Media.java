package alice.ep;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Media {
    @JsonProperty
    private String name;
    @JsonProperty
    private String key;
    @JsonProperty
    private String path;

    public Media() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "Media{" +
                "name='" + name + '\'' +
                ", key='" + key + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
