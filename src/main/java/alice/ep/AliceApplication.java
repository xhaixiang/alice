package alice.ep;

import alice.ep.conf.ServerConfiguration;
import alice.ep.monitor.DemoHealthCheck;
import alice.ep.startup.RegisterTask;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class AliceApplication extends Application<ServerConfiguration> {
    public static void main(String[] args) throws Exception {
        new AliceApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<ServerConfiguration> bootstrap) {

    }

    @Override
    public void run(ServerConfiguration configuration, Environment environment) throws Exception {
        final AliceResource resourceAlice = new AliceResource(configuration);
        environment.healthChecks().register("demo", new DemoHealthCheck());
        environment.jersey().register(resourceAlice);
        environment.admin().addTask(new RegisterTask(configuration));
    }

}
