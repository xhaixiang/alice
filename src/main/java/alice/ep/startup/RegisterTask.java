package alice.ep.startup;

import alice.ep.conf.ServerConfiguration;
import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.io.PrintWriter;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class RegisterTask extends Task {
    private static final Logger log = LoggerFactory.getLogger(RegisterTask.class);
    private ServerConfiguration conf;

    public RegisterTask(ServerConfiguration configuration) {
        super("register");
        this.conf = configuration;
    }

    @Override
    public void execute(ImmutableMultimap<String, String> parameters, PrintWriter output) throws Exception {
        register();

    }

    private void register() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://" + conf.getRoute().getHost() + ":" + conf.getRoute().getPort() + "/" + conf.getRoute().getContextPath())
                .build();
        RouteService routeService = restAdapter.create(RouteService.class);
        final MediaServer ss = new MediaServer(conf.getAlice());
        routeService.register(ss, new Callback<MediaServer>() {
            @Override
            public void success(MediaServer mediaServer, Response response) {
                log.info("succes register media server {} with url {}", ss, response.getUrl());
            }

            @Override
            public void failure(RetrofitError retrofitError) {
                log.info("error in register server url {}, with error {} ", retrofitError.getUrl(), retrofitError);
            }
        });
    }

}
