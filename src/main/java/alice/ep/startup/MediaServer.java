package alice.ep.startup;

import alice.ep.conf.AliceConfiguration;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public class MediaServer {
    private String name;
    private String host;
    private int port;
    private int size = 100;
    private int threshold = 80;
    private String protocol;
    private String broadCast;

    public MediaServer() {
    }

    public MediaServer(AliceConfiguration route) {
        this.name = route.getName();
        this.host = route.getHost();
        this.port = route.getPort();
        this.size = route.getSize();
        this.broadCast = route.getBroadcast();
        this.threshold = route.getThreshold();
        this.protocol = route.getProtocol();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getBroadCast() {
        return broadCast;
    }

    public void setBroadCast(String broadCast) {
        this.broadCast = broadCast;
    }

    @Override
    public String toString() {
        return "MediaServer{" +
                "name='" + name + '\'' +
                ", host='" + host + '\'' +
                ", port=" + port +
                ", size=" + size +
                ", threshold=" + threshold +
                ", protocol='" + protocol + '\'' +
                ", broadCast='" + broadCast + '\'' +
                '}';
    }
}
