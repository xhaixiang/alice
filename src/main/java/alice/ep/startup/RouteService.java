package alice.ep.startup;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by xuhaixiang on 7/21/14.
 */
public interface RouteService {

    @POST("/server/v1/register")
    void register(@Body MediaServer mediaServer, Callback<MediaServer> ms);

    @POST("/server/v1/finish/{key}")
    void finishRecording(@Path("key") String key);

}
