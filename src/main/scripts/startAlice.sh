#!/bin/sh
export JAVA_HOME=/home/genvision/apps/jdk
ALICE_ROOT=/home/genvision/apps/alice
cp /home/genvision/workspace/war/alice.jar $ALICE_ROOT
cp /home/genvision/workspace/conf/alice.yml $ALICE_ROOT
/home/genvision/apps/jdk/bin/java -jar $ALICE_ROOT/alice.jar server $ALICE_ROOT/alice.yml &
echo $! > $ALICE_ROOT/alice.pid
