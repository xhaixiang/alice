package alice.ep.core;

import alice.ep.Media;
import alice.ep.conf.AliceConfiguration;
import alice.ep.conf.MediaConfiguration;
import alice.ep.conf.ServerConfiguration;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;

@Ignore
public class CacheStoreTest {

    @Test
    public void play() throws IOException {
        CacheStore cs = new CacheStore();
        Media media = new Media();
        media.setPath("/Users/xuhaixiang/var/red/hdfsroot/");
        media.setName("abcd.mp4");
        ServerConfiguration configure = makeServerConf();
        cs.play(media, configure);
    }

    @Test
    public void playInRtmpPath() throws IOException {
        CacheStore cs = new CacheStore();
        Media media = new Media();
        media.setName("1.mp4");
        ServerConfiguration configure = makeServerConf();
        cs.play(media, configure);
    }

    @Test
    public void playInCachePath() throws IOException {
        CacheStore cs = new CacheStore();
        Media media = new Media();
        media.setName("2.mp4");
        ServerConfiguration configure = makeServerConf();
        cs.play(media, configure);
    }

    private ServerConfiguration makeServerConf() {
        ServerConfiguration sc = new ServerConfiguration();
        AliceConfiguration ac = getAliceConfiguration();
        MediaConfiguration mc = getMediaConfiguration();
        sc.setAlice(ac);
        sc.setMediaServer(mc);
        return sc;
    }

    private MediaConfiguration getMediaConfiguration() {
        MediaConfiguration mc = new MediaConfiguration();
        mc.setHost("1.2.3.4");
        mc.setPort(1395);
        mc.setDataPath("/Users/xuhaixiang/var/red/rtmproot");
        mc.setRepositoryPath("/Users/xuhaixiang/var/red/cacheroot");
        return mc;
    }

    private AliceConfiguration getAliceConfiguration() {
        AliceConfiguration ac = new AliceConfiguration();
        ac.setHost("localhost");
        ac.setPort(9000);
        ac.setProtocol("RTMP");
        ac.setScriptRoot("/Users/xuhaixiang/var/red/cmd");
        return ac;
    }
}
