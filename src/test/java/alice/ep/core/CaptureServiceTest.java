package alice.ep.core;

import alice.ep.Media;
import alice.ep.conf.AliceConfiguration;
import alice.ep.conf.MediaConfiguration;
import alice.ep.conf.ServerConfiguration;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

@Ignore
public class CaptureServiceTest {

    @Test
    public void convertTime() {
        CaptureService cs = new CaptureService();
        assertEquals(cs.convert(1234L), "00:20:34");
    }

    @Test
    public void capter(){
        Media media = new Media();
        media.setName("2.mp4");
        ServerConfiguration configure = makeServerConf();
        CaptureService cs = new CaptureService();
        String cp = cs.capture(media, 1234L, configure);
        System.out.println(cp);
    }

    private ServerConfiguration makeServerConf() {
        ServerConfiguration sc = new ServerConfiguration();
        AliceConfiguration ac = getAliceConfiguration();
        MediaConfiguration mc = getMediaConfiguration();
        sc.setAlice(ac);
        sc.setMediaServer(mc);
        return sc;
    }

    private MediaConfiguration getMediaConfiguration() {
        MediaConfiguration mc = new MediaConfiguration();
        mc.setHost("1.2.3.4");
        mc.setPort(1395);
        mc.setDataPath("/Users/xuhaixiang/var/red/rtmproot");
        mc.setRepositoryPath("/Users/xuhaixiang/var/red/cacheroot");
        return mc;
    }

    private AliceConfiguration getAliceConfiguration() {
        AliceConfiguration ac = new AliceConfiguration();
        ac.setHost("localhost");
        ac.setPort(9000);
        ac.setProtocol("RTMP");
        ac.setScriptRoot("/Users/xuhaixiang/var/red/cmd");
        return ac;
    }

}
